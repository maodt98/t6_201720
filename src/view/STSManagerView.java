package view;

import java.util.Scanner;

import controller.Controller;
import data_structures.LinearProbingHashST;
import data_structures.SeparateChainingHashST;
import vo.Stop_timesVO;
import vo.TripsVO;

public class STSManagerView {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();

			int option = sc.nextInt();

			switch(option){
			case 1:
				Controller.cargar();;
				break;
			case 2:
				System.out.println("Ingrese el id de la parada");
				String idTripCas = sc.next();
				Integer i1 = Integer.parseInt(idTripCas);

				LinearProbingHashST<Integer, TripsVO> ha = Controller.viajesParada(i1);
				for(Integer i:ha.keys()){
					TripsVO trip = ha.get(i);
					System.out.println("TriId:"+trip.getTrip_id()+" | Tiempo de llegada: "+i);
					System.out.println("\n");
				break;
				}
				
			case 3:
				System.out.println("Ingrese el id del primer viaje");
				String idTripCase3 = sc.next();
				Integer id1 = Integer.parseInt(idTripCase3);

				System.out.println("Ingrese el id del segundo viaje");
				String id2TripCase3 = sc.next();
				Integer id2 = Integer.parseInt(id2TripCase3);

				SeparateChainingHashST<Integer, Stop_timesVO> hashT = Controller.paradasCompartidas(id1, id2);
				for(Integer i:hashT.keys()){
					Stop_timesVO stop = hashT.get(i);
					System.out.println("StopId: "+i+" | TripId: "+stop.getTrip_id()+" | Tiempo de llegada: "+stop.getArrival_time());
					System.out.println("\n");
				}
				break;
			}
		}
	}
	public static void printMenu(){
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		System.out.println("1. Cargar data");
		System.out.println("2. ");
		System.out.println("3. Dar paradas que son compartidas por dos viajes");

	}
}
