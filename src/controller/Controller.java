package controller;

import data_structures.LinearProbingHashST;
import data_structures.SeparateChainingHashST;
import logic.STSManager;
import vo.Stop_timesVO;
import vo.TripsVO;

public class Controller {

	private static STSManager man = new STSManager();
	
	public static void cargar(){
		man.ITScargarGTFS();
	}
	
	public static SeparateChainingHashST<Integer, Stop_timesVO> paradasCompartidas(int trip1, int trip2){
		return man.paradasCompartidas(trip1, trip2);
	}
	public static LinearProbingHashST<Integer, TripsVO> viajesParada(int idParada)
	{
		return man.viajesParada(idParada);
	}
}
