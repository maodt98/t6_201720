package data_structures;

public class SeparateChainingHashST<Key, Value> {
	private static final int CAPACIDAD_INICIAL = 4;

    private int n;                                
    private int m;                                
    private SequentialSearchST<Key, Value>[] st;  

    public SeparateChainingHashST() {
        this(CAPACIDAD_INICIAL);
    } 

    public SeparateChainingHashST(int m) {
        this.m = m;
        st = (SequentialSearchST<Key, Value>[]) new SequentialSearchST[m];
        for (int i = 0; i < m; i++)
            st[i] = new SequentialSearchST<Key, Value>();
    } 

    private void resize(int chains) {
        SeparateChainingHashST<Key, Value> temp = new SeparateChainingHashST<Key, Value>(chains);
        for (int i = 0; i < m; i++) {
            for (Key key : st[i].keys()) {
                temp.put(key, st[i].get(key));
            }
        }
        this.m  = temp.m;
        this.n  = temp.n;
        this.st = temp.st;
    }

    private int hash(Key key) {
        return (key.hashCode() & 0x7fffffff) % m;
    } 

    public int size() {
        return n;
    } 

    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean contains(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to contains() is null");
        return get(key) != null;
    } 

    public Value get(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to get() is null");
        int i = hash(key);
        return st[i].get(key);
    } 

    public void put(Key key, Value val) {
        if (key == null) throw new IllegalArgumentException("first argument to put() is null");
        if (val == null) {
            delete(key);
            return;
        }

        if (n >= 10*m) resize(2*m);

        int i = hash(key);
        if (!st[i].contains(key)) n++;
        st[i].put(key, val);
    } 

    public void delete(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to delete() is null");

        int i = hash(key);
        if (st[i].contains(key)) n--;
        st[i].delete(key);

        if (m > CAPACIDAD_INICIAL && n <= 2*m) resize(m/2);
    } 

    public Iterable<Key> keys() {
        LinkedList<Key> list = new LinkedList<>();
        for (int i = 0; i < m; i++) {
            for (Key key : st[i].keys())
                list.add(key);
        }
        return list;
    } 
    
   /** public static void main(String[] args) { 
        SeparateChainingHashST<String, Integer> st = new SeparateChainingHashST<String, Integer>();
        String[] arr = {"hola", "asdas", "pe", "dsadasd"};
        for (int i = 0;i<arr.length ; i++) {
            String key = arr[i];
            st.put(key, i);
        }

        for (String s : st.keys()) 
            System.out.println(s + " " + st.get(s)); 

    }**/

}
