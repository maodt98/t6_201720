package data_structures;

public class RadixSort<T>
{

	// Organiza los numeros comenzando con el digito menos significativo.

	public static MyInteger[] sort(MyInteger[] input){

		for(int place=1; place <= maxDigit(input); place *= 10){

			// Utiliza el CountingSort en cada place
			input = countingSort(input, place);
		}

		return input;
	}

	private static  int maxDigit(MyInteger[] l){
		int max = 0;
		for(int i = 0; i< l.length; i++){
			if(l[i].intValue() > max){
				max = l[i].intValue();
			}
		}
		return max;
	}


	private static  MyInteger[] countingSort(MyInteger[] input, int place){
		MyInteger[] out = new MyInteger[input.length];

		int[] count = new int[10];

		for(int i=0; i < input.length; i++){
			int digit = getDigit(input[i].intValue(), place);
			count[digit] += 1;

		}

		for(int i=1; i < count.length; i++){
			count[i] += count[i-1];
		}

		for(int i = input.length-1; i >= 0; i--){
			int digit = getDigit(input[i].intValue(), place);

			out[count[digit]-1] = input[i];
			count[digit]--;
		}

		return out;

	}

	private  static int getDigit(int value, int digitPlace){
		return ((value/digitPlace ) % 10);
	}

}