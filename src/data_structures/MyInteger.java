package data_structures;

public class MyInteger<T> {

	private int value;
	
	private int value2;
	
	private double doubleValue;
	
	private double doubleValue2;

	private T data;

	public MyInteger(int i){
		this.value = i;
		this.value2=0;
		this.doubleValue = 0.0;
		this.doubleValue2 = 0.0;
		data = null;
	}
	public MyInteger(int i , double j){
		this.setDoubleValue(j);
		this.value2=0;
		this.doubleValue2 = 0.0;
		data = null;
		value = 0;
	}
	public MyInteger(int i, double j, double z){
		this.value = i;
		this.value2 = 0;
		this.doubleValue = j;
		this.doubleValue2 = z;
		data = null;
	}
	public MyInteger(int i, int j){
		this.value = i;
		this.value2 = j;
		this.doubleValue = 0.0;
		this.doubleValue2 = 0.0;
		data = null;
	}

	public int intValue() {
		return value;
	}

	public void setValue(int i){
		this.value = i;
	}

	public T getData(){
		return data;
	}

	public void setData(T pData){
		this.data = pData;
	}

	public void sumValue(int pInt){
		value += pInt;
	}
	public int getValue()
	{
		return value;
	}
	/**
	 * @return the doubleValue
	 */
	public double getDoubleValue() {
		return doubleValue;
	}
	/**
	 * @param doubleValue the doubleValue to set
	 */
	public void setDoubleValue(double doubleValue) {
		this.doubleValue = doubleValue;
	}
	
	public void sumDoubleValue(double pDouble){
		doubleValue += pDouble;
	}
	/**
	 * @return the value2
	 */
	public int getValue2() {
		return value2;
	}
	/**
	 * @param value2 the value2 to set
	 */
	public void setValue2(int value2) {
		this.value2 = value2;
	}
	
	public void sumValue2(int value){
		value2 += value;
	}
	/**
	 * @return the doubleValue2
	 */
	public double getDoubleValue2() {
		return doubleValue2;
	}
	/**
	 * @param doubleValue2 the doubleValue2 to set
	 */
	public void setDoubleValue2(double doubleValue2) {
		this.doubleValue2 = doubleValue2;
	}
	
	public void sumDoubleValue2(double doubleValue){
		this.doubleValue2 += doubleValue;
	}
}
