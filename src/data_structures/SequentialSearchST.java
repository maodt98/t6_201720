package data_structures;

public class SequentialSearchST<Key, Value> {
	private int n;           
	private NodeHT first;      


	/**private class NodeHT {
        private Key key;
        private Value val;
        private NodeHT next;

        public NodeHT(Key key, Value val, NodeHT next)  {
            this.key  = key;
            this.val  = val;
            this.next = next;
        }
    }**/


	public SequentialSearchST() {
	}


	public int size() {
		return n;
	}


	public boolean isEmpty() {
		return size() == 0;
	}


	public boolean contains(Key key) {
		return get(key) != null;
	}


	public Value get(Key key) {
		for (NodeHT<Key, Value> x = first; x != null; x = x.getNext()) {
			if (key.equals(x.getKey()))
				return x.getVal();
		}
		return null;
	}


	public void put(Key key, Value val) {
		if (val == null) {
			delete(key);
			return;
		}

		for (NodeHT<Key, Value> x = first; x != null; x = x.getNext()) {
			if (key.equals(x.getKey())) {
				Value v = x.getVal();
				v = val;
				return;
			}
		}
		first = new NodeHT(key, val, first);
		n++;
	}


	public void delete(Key key) {
		first = delete(first, key);
	}

	private NodeHT delete(NodeHT x, Key key) {
		if (x == null) return null;
		if (key.equals(x.getKey())) {
			n--;
			return x.getNext();
		}
		NodeHT next = x.getNext();
		next = delete(x.getNext(), key);
		return x;
	}

	public Iterable<Key> keys()  {
		LinkedList<Key> list = new LinkedList<Key>();
		for (NodeHT<Key, Value> x = first; x != null; x = x.getNext())
			list.add(x.getKey());
		return list;
	}


}
