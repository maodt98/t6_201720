package data_structures;

public class HashTable<T> 
{
	private static final int SIZE = 97;
	private Pair[] map = null;



	public HashTable(int N){
		map = new Pair[N];
		for(int i =0; i < N ; i++){
			map[i]=null;
		}
	}

	public void put(int key, int value) throws HashtableException {
		int hash = key % SIZE;
		int count = 0;
		while(map[hash] != null && map[hash].getKey() != key){ 
			hash = (hash + 1) % SIZE;
			if(count == SIZE)
				throw new HashtableException("Table full");
			count++;
		}
		map[hash]= new Pair(key,value);
	}

	public int get(int key) throws HashtableException{
		int hash = key % SIZE;
		int count = 0;

		while(map[hash] != null && map[hash].getKey() != key){
			hash = (hash+1) % SIZE;
			if(count == SIZE)
				throw new HashtableException("No hay una llave que coincida en la tabla");
			count++;
		}
		if(map[hash] == null)
			throw new HashtableException("No hay una llave que coincida en la tabla");

		return map[hash].getValue();

	}

}
