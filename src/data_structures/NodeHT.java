package data_structures;

public class NodeHT<Key, Value> {

	private Key key;
	private Value val;
    private NodeHT next;

    public NodeHT(Key key, Value val, NodeHT next)  {
        this.key  = key;
        this.val  = val;
        this.next = next;
    }
    public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}

	public Value getVal() {
		return val;
	}

	public void setVal(Value val) {
		this.val = val;
	}

	public NodeHT getNext() {
		return next;
	}

	public void setNext(NodeHT next) {
		this.next = next;
	}

}
