package logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import data_structures.LinearProbingHashST;
import data_structures.LinkedList;
import data_structures.MyInteger;
import data_structures.Node;
import data_structures.RadixSort;
import data_structures.SeparateChainingHashST;
import vo.Stop_timesVO;
import vo.StopsVO;
import vo.TripsVO;


public class STSManager<T> {



	private LinkedList<Stop_timesVO> stopTimes = new LinkedList<>();
	SeparateChainingHashST<Integer, TripsVO> trips = new SeparateChainingHashST<Integer, TripsVO>();
	

	public void ITScargarGTFS() {


		loadStopTimes();
		System.out.println("-----------------Carg� efectivamente StopTimes-----------------");
		loadTrips();
		System.out.println("-------------------Carg� efectivamente Trips-------------------");
	}




	public void loadStopTimes() {
		String file = "./data/stop_times.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				Stop_timesVO x = new Stop_timesVO(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), datos[5], Byte.parseByte(datos[6]), Byte.parseByte(datos[7]), (datos.length>8)?Double.parseDouble(datos[8]):0);
				stopTimes.add(x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadTrips() {
		String file = "./data/trips.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				TripsVO x = new TripsVO(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]), Integer.parseInt(datos[2]), datos[3], datos[4], Byte.parseByte(datos[5]), Integer.parseInt(datos[6]), Integer.parseInt(datos[7]), Byte.parseByte(datos[8]), Byte.parseByte(datos[9]));
				trips.put(x.getTrip_id(), x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public LinearProbingHashST<Integer, TripsVO> viajesParada(int idParada)
	{
		LinearProbingHashST<Integer, TripsVO> fin = new LinearProbingHashST<Integer, TripsVO>();
		ArrayList<Integer> trips2 = new ArrayList<Integer>();
		LinearProbingHashST<Integer, TripsVO> lTrips = new LinearProbingHashST<Integer, TripsVO>();
		LinearProbingHashST<Integer, TripsVO> lTrips2 = new LinearProbingHashST<Integer, TripsVO>();
		ArrayList<Stop_timesVO>stops = new ArrayList<>();
		
		for(Stop_timesVO st:stopTimes){
			if(st.getStop_id()== idParada ){
				trips2.add(st.getTrip_id());
				stops.add(st);
			}
			Iterator kys  = trips.keys().iterator();
			while(kys.hasNext())
			{
				TripsVO tripAct = (TripsVO) kys.next();
				for(int j = 0; j < trips2.size();j++){

					for(int k = 0; k<stops.size();k++)
					{
							if(trips2.get(j) == tripAct.getTrip_id()){
							if(stops.get(k).getTrip_id()==trips.get(j).getTrip_id()){
								lTrips.put(horaAIntSeg(stops.get(k).getArrival_time()), tripAct);
							}
						}
					}
				}

			}
			LinkedList<MyInteger<TripsVO>> l = new LinkedList<>();
			for(Integer trips:lTrips.keys())
			{
				TripsVO i = lTrips.get(trips);
				if(i != null)
				{
					MyInteger<TripsVO> my = new MyInteger<TripsVO>(trips.intValue());
					my.setData(i);
					l.add(my);
				}
			}

			MyInteger<TripsVO>[] arrL = (MyInteger<TripsVO>[]) listaMyIntegerArreglo(l);
			arrL= RadixSort.sort(arrL);

			for(int i = 0; i<arrL.length; i++){
				MyInteger<TripsVO> m = arrL[i];
				fin.put(m.getValue(), m.getData());
			}
		}
			return fin;
		
		
	}
	public SeparateChainingHashST<Integer, Stop_timesVO> paradasCompartidas(int trip1, int trip2)
	{
		SeparateChainingHashST<Integer, Stop_timesVO> retorno = new SeparateChainingHashST<Integer, Stop_timesVO>();

		TripsVO t1 = trips.get(trip1);
		TripsVO t2 = trips.get(trip2);

		SeparateChainingHashST<Integer, Stop_timesVO> lStops1 = new SeparateChainingHashST<Integer, Stop_timesVO>();
		SeparateChainingHashST<Integer, Stop_timesVO> lStops2 = new SeparateChainingHashST<Integer, Stop_timesVO>();
		for(Stop_timesVO stop:stopTimes){
			if(stop.getTrip_id().intValue() == trip1 ){
				lStops1.put(stop.getStop_id(), stop);
			}
			else if(stop.getTrip_id().intValue() == trip2){
				lStops2.put(stop.getStop_id(), stop);
			}
		}

		LinkedList<MyInteger<Stop_timesVO>> l = new LinkedList<>();
		for(Integer stopId:lStops1.keys())
		{
			Stop_timesVO i = lStops2.get(stopId);
			if(i != null)
			{
				MyInteger<Stop_timesVO> my = new MyInteger<Stop_timesVO>(horaAIntSeg(i.getArrival_time()));
				i.setHoraEnSeg(horaAIntSeg(i.getArrival_time()));
				my.setData(i);
				l.add(my);
			}
		}

		MyInteger<Stop_timesVO>[] arrL = (MyInteger<Stop_timesVO>[]) listaMyIntegerArreglo(l);
		arrL= RadixSort.sort(arrL);

		for(int i = 0; i<arrL.length; i++){
			MyInteger<Stop_timesVO> m = arrL[i];
			retorno.put(m.getData().getStop_id(), m.getData());
		}

		return retorno;


	}

	//---------------------------------------------------------
	//-------------------  Metodos Comp------------------------
	//---------------------------------------------------------
	public Integer horaAIntSeg(String hora){
		hora = hora.replaceAll("\\s+","");
		String[] partesH = hora.split(":");
		Integer horaA = Integer.parseInt(partesH[0])*3600;
		Integer min = Integer.parseInt(partesH[1])*60;
		Integer seg = Integer.parseInt(partesH[2]);
		Integer intHora = horaA+min+seg;

		return intHora;
	}
	public MyInteger<T>[] listaArreglo(LinkedList pLista){

		MyInteger<T>[] arr = new MyInteger[pLista.getSize()];
		Node<T> act = pLista.getHead();

		for(int i = 0; i<pLista.getSize()&& act != null; i++){
			MyInteger<T> n = new MyInteger<>(i);
			n.setData(act.getData());
			arr[i] = n;
			act = act.getNext();
		}

		return arr;

	}

	public MyInteger<T>[] listaMyIntegerArreglo(LinkedList pLista){

		MyInteger<T>[] arr = new MyInteger[pLista.getSize()];
		Node<MyInteger<T>> act = pLista.getHead();

		for(int i = 0; i<pLista.getSize()&& act != null; i++){
			MyInteger<T> n = new MyInteger<>(0,0);
			n.setData(act.getData().getData());
			n.setValue(act.getData().intValue());
			n.setValue2(act.getData().getValue2());
			arr[i] = n;
			act = act.getNext();
		}

		return arr;

	}

}
